package yuri.lechshnko.com.roomexample.app;

import android.app.Application;
import android.arch.persistence.room.Room;

import yuri.lechshnko.com.roomexample.BuildConfig;
import yuri.lechshnko.com.roomexample.Const;
import yuri.lechshnko.com.roomexample.remote.RestService;
import yuri.lechshnko.com.roomexample.room.LocalServiceEntry;
import yuri.lechshnko.com.roomexample.room.PersonDataBase;

public class App extends Application {
    private static LocalServiceEntry dao;
    private static RestService service;

    @Override
    public void onCreate() {
        super.onCreate();
        dao = provideLocaleDao(provideRoomDataBase());
        service = new RestService.Builder()
                .gson()
                .service(BuildConfig.SERVER_URL)
                .build();
    }

    private PersonDataBase provideRoomDataBase() {
        return Room.databaseBuilder(
                this, PersonDataBase.class, Const.NAME_DAO)
                .build();
    }

    private LocalServiceEntry provideLocaleDao(PersonDataBase dao) {
        return dao.getPaxDao();
    }

    public static LocalServiceEntry getDao() {
        return dao;
    }

}
