package yuri.lechshnko.com.roomexample.domain;

import android.util.Log;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import yuri.lechshnko.com.roomexample.Mock;
import yuri.lechshnko.com.roomexample.data.Repository;
import yuri.lechshnko.com.roomexample.data.RepositoryContract;
import yuri.lechshnko.com.roomexample.domain.base.BaseInteractor;
import yuri.lechshnko.com.roomexample.room.Entry;

public class Interactor extends BaseInteractor implements InteractorContract {
    private RepositoryContract repository;

    public Interactor() {
        repository = new Repository();
    }

    @Override
    public Single<List<Entry>> getList() {
        return repository.queryEntryList()
                .compose(applySingleSchedulers());
    }

    @Override
    public void insertMockBD() {
        repository.getMock()
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableSingleObserver<List<Entry>>() {
                    @Override
                    public void onSuccess(List<Entry> list) {
                        repository.insertListEntry(list);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void insertListEntry(List<Entry> list) {
        Completable.fromAction(() -> repository.insertListEntry(list))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

}
