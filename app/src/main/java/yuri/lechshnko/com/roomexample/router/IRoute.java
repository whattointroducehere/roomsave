package yuri.lechshnko.com.roomexample.router;

import android.support.v4.app.Fragment;

public interface IRoute {
    void transitionFragment(Fragment fragment, int contener);
}
