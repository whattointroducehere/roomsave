package yuri.lechshnko.com.roomexample.data;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import yuri.lechshnko.com.roomexample.room.Entry;

public interface RepositoryContract {

    Single<List<Entry>> getMock();

    void insertListEntry(List<Entry> list);

    void insertEntry(Entry entry);

    Single<List<Entry>> queryEntryList();

    Single<Entry> queryEntry(String key);
}
