package yuri.lechshnko.com.roomexample.presentational.activity;



import yuri.lechshnko.com.roomexample.presentational.base.BasePresenter;


public interface PresenterContract {
    interface View {
        void openDrawer();

        void closeDrawer();


    }

    interface Presenter extends BasePresenter<View> {
        void init();

        void openDrawer();

        void closeDrawer();

        void openAddUser();

        void transition();
    }

}
