package yuri.lechshnko.com.roomexample.presentational.activity;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;

import java.util.List;
import yuri.lechshnko.com.roomexample.Adapter;
import yuri.lechshnko.com.roomexample.databinding.ActivityMainBinding;
import yuri.lechshnko.com.roomexample.R;
import yuri.lechshnko.com.roomexample.presentational.base.BaseActivity;
import yuri.lechshnko.com.roomexample.presentational.base.BasePresenter;
import yuri.lechshnko.com.roomexample.room.Entry;
import yuri.lechshnko.com.roomexample.router.IRoute;
import yuri.lechshnko.com.roomexample.router.Route;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements PresenterContract.View, IRoute {
    private PresenterContract.Presenter presenter;
    private Adapter adapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new Presenter();
        getBinding().setEvent(presenter);
        Route.getInstance().startView(this);
        presenter.init();
    }

    @Override
    protected void onStartView() {
        presenter.starView(this);
    }

    @Override
    protected void onDestroyView() {
        presenter.stopView();
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void transitionFragment(Fragment fragment, int contener) {
        transactionFragmentWithBackStack(fragment,contener);
    }

//
//    @Override
//    public void initDisplay(List<Entry> list) {
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        getBinding().rvMain.setLayoutManager(layoutManager);
//        adapter = new Adapter(list,presenter);
//        getBinding().rvMain.setAdapter(adapter);
//    }



    @Override
    public void openDrawer() {
        getBinding().drawerLayout.openDrawer(Gravity.START);
    }

    @Override
    public void closeDrawer() {
        getBinding().drawerLayout.closeDrawers();
    }
}
