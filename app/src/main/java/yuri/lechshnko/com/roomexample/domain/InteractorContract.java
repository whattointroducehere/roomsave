package yuri.lechshnko.com.roomexample.domain;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import yuri.lechshnko.com.roomexample.room.Entry;


public interface InteractorContract {
    Single<List<Entry>> getList();

    void insertMockBD();

    void insertListEntry(List<Entry> list);

}
