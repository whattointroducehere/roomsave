package yuri.lechshnko.com.roomexample.router;

import yuri.lechshnko.com.roomexample.R;
import yuri.lechshnko.com.roomexample.presentational.fragments.fragmentadduser.FragmentAdd;

public class Route {
        private static Route instance;
        private IRoute view;

        private Route() {
        }

        public static synchronized Route getInstance(){
            if(instance == null){
                return instance = new Route();
            }else {
                return instance;
            }
        }

        public  void transactionFradd(){
            view.transitionFragment(FragmentAdd.newInstance(), R.id.main_content);
        }



        public void startView(IRoute view){ this.view = view;
        }

        public void stopView(){
            if(view != null) view = null;
        }

    }

