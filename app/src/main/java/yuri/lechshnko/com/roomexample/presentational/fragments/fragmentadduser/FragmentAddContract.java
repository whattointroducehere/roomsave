package yuri.lechshnko.com.roomexample.presentational.fragments.fragmentadduser;

import yuri.lechshnko.com.roomexample.presentational.base.BasePresenter;
import yuri.lechshnko.com.roomexample.presentational.base.BaseViewFragmentContract;

public interface FragmentAddContract {
    interface View extends BaseViewFragmentContract {

    }

    interface Listener extends BasePresenter<View> {
        void eventClick();
    }
}
