package yuri.lechshnko.com.roomexample.presentational.base;


public interface BaseViewActivityContract {
    void onMessage(String message);

    void showProgress();

    void hideProgress();
}
