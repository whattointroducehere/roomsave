package yuri.lechshnko.com.roomexample.presentational.fragments.fragmentadduser;

import android.os.Bundle;
import android.widget.Toast;

import yuri.lechshnko.com.roomexample.R;
import yuri.lechshnko.com.roomexample.databinding.FragmentAddUserBinding;
import yuri.lechshnko.com.roomexample.presentational.base.BaseFragment;
import yuri.lechshnko.com.roomexample.presentational.base.BasePresenter;

public class FragmentAdd extends BaseFragment<FragmentAddUserBinding> implements FragmentAddContract.View {

    private FragmentAddContract.Listener presenter;

    public FragmentAdd()  {
    }

    public static FragmentAdd newInstance() {
        FragmentAdd fragment = new FragmentAdd();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initFragmentView() {

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_add_user;
    }


    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.starView(this);
    }

    @Override
    protected void pauseFragment() {

    }

    @Override
    protected void stopFragment() {

    }

    @Override
    protected void destroyFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
    }



    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

}

