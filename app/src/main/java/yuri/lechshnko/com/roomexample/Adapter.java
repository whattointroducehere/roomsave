package yuri.lechshnko.com.roomexample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import yuri.lechshnko.com.roomexample.presentational.activity.PresenterContract;
import yuri.lechshnko.com.roomexample.room.Entry;

public class Adapter extends RecyclerView.Adapter<ViewHolderMain> {
     private List<Entry> list;
     private PresenterContract.Presenter listener;

    public Adapter(List<Entry> list,PresenterContract.Presenter listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolderMain onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new ViewHolderMain(view,viewGroup.getContext(),listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderMain viewHolderMain, int i) {
            viewHolderMain.bind(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
