package yuri.lechshnko.com.roomexample.presentational.activity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import yuri.lechshnko.com.roomexample.domain.Interactor;
import yuri.lechshnko.com.roomexample.domain.InteractorContract;
import yuri.lechshnko.com.roomexample.room.Entry;
import yuri.lechshnko.com.roomexample.router.Route;


public class Presenter implements PresenterContract.Presenter {
    private PresenterContract.View view;
    private CompositeDisposable disposable;
    private InteractorContract interactor;

    public Presenter() {
        disposable = new CompositeDisposable();
        interactor = new Interactor();
    }

    public void init(){
//        interactor.insertMockBD();
//        Observable<List<Entry>> ob = interactor.getList().toObservable();
//        disposable.add(ob.subscribe(v -> {
//                    view.initDisplay(v);
//                }));
    }

    @Override
    public void openDrawer() {
        view.openDrawer();
    }

    @Override
    public void closeDrawer() {
        view.closeDrawer();
    }

    @Override
    public void openAddUser() {
        Route.getInstance().transactionFradd();
    }

    @Override
    public void transition() {

    }


    @Override
    public void starView(PresenterContract.View view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        view = null;
        disposable.clear();
    }
}
