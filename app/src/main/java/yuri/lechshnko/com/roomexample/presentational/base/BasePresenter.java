package yuri.lechshnko.com.roomexample.presentational.base;

public interface BasePresenter<T> {
        void starView(T view);

        void stopView();

}
