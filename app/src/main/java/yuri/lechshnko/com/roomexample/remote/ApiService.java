package yuri.lechshnko.com.roomexample.remote;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @GET("/api/cmd")
    Single<Response<ResponseBody>> getXZ();

    @POST("/api/cmd/{as}")
    Single<ResponseBody> updateSettings(@Path("as") String val, @Header("X-Terminal-SN") String sn, @Body ResponseBody requestExample);
}
