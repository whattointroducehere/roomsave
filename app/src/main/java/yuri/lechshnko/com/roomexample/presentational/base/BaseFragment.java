package yuri.lechshnko.com.roomexample.presentational.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment <Binding extends ViewDataBinding> extends Fragment {
    private Binding binding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStop() {
        stopFragment();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onPause() {
        pauseFragment();
        super.onPause();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initFragmentView();
        return binding.getRoot();
    }

    protected abstract void initFragmentView();

    protected Binding getBinding() {
        return binding;
    }

    @LayoutRes
    protected abstract int getLayoutRes();


    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract void pauseFragment();

    protected abstract void stopFragment();

    protected abstract void destroyFragment();

    protected abstract BasePresenter getPresenter();

    @Override
    public void onDestroy() {
        if (getPresenter() != null) {
            getPresenter().stopView();
        }
        destroyFragment();
        super.onDestroy();
    }
}
