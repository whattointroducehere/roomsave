package yuri.lechshnko.com.roomexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import yuri.lechshnko.com.roomexample.presentational.activity.PresenterContract;
import yuri.lechshnko.com.roomexample.room.Entry;


public class ViewHolderMain extends RecyclerView.ViewHolder {
    private AppCompatTextView firstName;
    private AppCompatTextView lastName;
    private AppCompatTextView age;
    private RoundedImageView riv;
    private Context context;

    public ViewHolderMain(@NonNull View itemView, Context context, PresenterContract.Presenter listener) {
        super(itemView);
        this.context = context;
        firstName = itemView.findViewById(R.id.textFirstName);
        lastName = itemView.findViewById(R.id.textLastName);
        age = itemView.findViewById(R.id.textAge);
        riv = itemView.findViewById(R.id.image);

    }


    public void bind(Entry item) {
        if (item != null) {
            firstName.setText(item.getFName());
            lastName.setText(item.getLName());
            age.setText(String.valueOf(item.getAge()));
            RequestOptions ro = new RequestOptions() // TODO узнать что это?
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .override(300, 300)
                    .fitCenter();
//            GlideApp.with(itemView.getContext())
//                    .load(item.getUrl())
//                    .into(riv);
            Glide.with(itemView)
                    .load(item.getUrl())
                    .centerCrop()
                    .into(riv);

        }
    }
}
