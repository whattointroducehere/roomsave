package yuri.lechshnko.com.roomexample;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import yuri.lechshnko.com.roomexample.room.Entry;

public class Mock {
    private List<Entry> list;
    private static Mock instance;

    private Mock() {
        list = new ArrayList<>();
    }

    public static synchronized Mock getInstance(){
        if(instance == null){
            instance = new Mock();
        }
        return instance;
    }

    public Single<List<Entry>> getList(){
        list.add(new Entry("Black","Cat",20,"https://i.gifer.com/origin/52/523e07e0ce014e99ccb95a9667dff742_w200.gif"));
        list.add(new Entry("Yellow","Car",25,"https://i.gifer.com/origin/52/523e07e0ce014e99ccb95a9667dff742_w200.gif"));
        list.add(new Entry("White","Color",32,"https://i.gifer.com/origin/52/523e07e0ce014e99ccb95a9667dff742_w200.gif"));
        list.add(new Entry("Black","Car",45,"https://i.gifer.com/origin/52/523e07e0ce014e99ccb95a9667dff742_w200.gif"));
        list.add(new Entry("Red","Ball",87,"https://i.gifer.com/origin/52/523e07e0ce014e99ccb95a9667dff742_w200.gif"));
        return Single.just(list);
    }
}
