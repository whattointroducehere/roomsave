package yuri.lechshnko.com.roomexample.presentational.base;

public interface BaseViewFragmentContract {
    void onMessage(String message);

    void showProgress();

    void hideProgress();
}
