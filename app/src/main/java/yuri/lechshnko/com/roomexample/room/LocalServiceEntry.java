package yuri.lechshnko.com.roomexample.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface LocalServiceEntry {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListEntry(List<Entry> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEntry(Entry entry);

    @Query("select * from entry")
    Single<List<Entry>> queryEntryList();

    @Query("SELECT * FROM entry WHERE lName IS :lName")
    Single<Entry> queryEntry(String lName);
}
