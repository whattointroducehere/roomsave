package yuri.lechshnko.com.roomexample.room;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import java.util.Objects;

@Entity(tableName = "entry",primaryKeys = {"lName"})
public class Entry {
    @NonNull
    private String lName;
    private String fName;
    private String url;
    private Integer age;

    @Ignore
    public Entry() {
    }

    public Entry(@NonNull String lName, String fName, int age, String url) {
        this.lName = lName;
        this.fName = fName;
        this.url = url;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "lName='" + lName + '\'' +
                ", fName='" + fName + '\'' +
                '}';
    }

    @NonNull
    public String getLName() {
        return lName;
    }

    public String getFName() {
        return fName;
    }

    public String getUrl() {
        return url;
    }

    public Integer getAge() {
        return age;
    }
}