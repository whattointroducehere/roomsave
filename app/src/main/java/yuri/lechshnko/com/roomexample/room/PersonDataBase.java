package yuri.lechshnko.com.roomexample.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Entry.class}, version =1,exportSchema = false)
public abstract class PersonDataBase extends RoomDatabase {
    public abstract LocalServiceEntry getPaxDao();
}
