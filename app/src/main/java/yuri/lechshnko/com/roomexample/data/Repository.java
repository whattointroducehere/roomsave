package yuri.lechshnko.com.roomexample.data;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import yuri.lechshnko.com.roomexample.Mock;
import yuri.lechshnko.com.roomexample.app.App;
import yuri.lechshnko.com.roomexample.room.Entry;

public class Repository implements RepositoryContract {
    @Override
    public Single<List<Entry>> getMock() {
        return Mock.getInstance().getList();
    }

    @Override
    public void insertListEntry(List<Entry> list) {
         App.getDao().insertListEntry(list);
    }




    @Override
    public void insertEntry(Entry entry) {
        App.getDao().insertEntry(entry);
    }

    @Override
    public Single<List<Entry>> queryEntryList() {
        return App.getDao().queryEntryList();
    }

    @Override
    public Single<Entry> queryEntry(String key) {
        return App.getDao().queryEntry(key);
    }
}
